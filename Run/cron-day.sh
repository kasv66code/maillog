#!/bin/bash
cd "/home/user/MailLog/" || exit

Backup() {
	/bin/tar -czf "Backup/$1" "${@:2}" 2>/dev/null
	Run/uDate "Backup/$1" -pml_ "-s_${1}.tgz"
}

echo "Delete ..."
Run/uDelete -d=2 logs
Run/uDelete -d=2 Backup
rm -f ./tmp/*

echo "Run ..."
gunzip -c /var/log/maillog.processed.1.gz > /home/user/MailLog/tmp/maillog.txt || exit
Run/MailCollect

#echo "Backup..."
Backup soft Source Run
Backup data Data

echo "Successfull !"
