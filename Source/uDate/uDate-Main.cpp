// uDate-Main.cpp									2019-09-17


#include "../cfgMC.hpp"

#ifdef _WIN32
    char* getcwd(char* buf, size_t size);
#endif


#define _SCRIPT_NAME	"uDate"



static void	getWorkMode(const char *Sp, char &workMode)
{
    for (; *Sp; Sp++) {
	if (*Sp == '-' || *Sp == '/') continue;
	break;
    }
    switch (*Sp) {
	case 'h':
	    workMode = 'h';
	    break;
	case 'f':
	    workMode = 'f';
	    break;
    }
}

// ------------------------------------------------------------------------------------------------

static const char *sPrefix;
static const char *sSuffix;
static const char *sFile;

static void	readParam(const char *S)
{
    if (!S || !*S) return;

    if (*S == '-') {
	S++;
	switch (*S) {
	    case 'p':
		sPrefix = S+1;
		break;
	    case 's':
		sSuffix = S+1;
		break;
	}
    } else {
	sFile = S;
    }
}

int	main(int argc, char** argv)
{
    __logInit(def_Folder_logs, _SCRIPT_NAME);
    setSignals(1);

    for (int i=1; i<argc; i++) {
	readParam(argv[i]);
    }

    if (!sPrefix) sPrefix = "";
    if (!sSuffix) sSuffix = "";

    char currDir[FileName_MaxSize];
    if (!getcwd(currDir, sizeof(currDir)-1)) {
        __logErr("getcwd");
	return 0;
    }

    if (!sFile || !*sFile) {
	if (!sFile) sFile = "(null)";
	else if (!*sFile) sFile = "''";
        __logErr("params: sFile=%s", sFile);
	return 0;
    }

    char fnIn[FileName_MaxSize];
    char fnOut[FileName_MaxSize];

    if (*sFile == '/') {
	strcpy(fnIn, sFile);
    } else {
	_fileNameMake(fileNameMakeMode_makeOnly, fnIn, currDir, sFile, NULL);
    }

    if (!_fileCheck(fnIn)) {
        __logErr("fnIn=%s", fnIn);
	return 0;
    }
    strcpy(fnOut, fnIn);

    char *sx = strrchr(fnOut, '/');
    if (!sx) {
        __logErr("no slash fnOut=%s", fnOut);
	return 0;
    }
    sx++;

    time_t  curr_time;
    time(&curr_time);

    struct tm T;
    localtime_r(&curr_time, &T);
    sprintf(sx, "%s%02d%02d%02d_%02d%s", sPrefix, T.tm_year % 100, T.tm_mon + 1, T.tm_mday, T.tm_hour, sSuffix);

    bool R = _fileRename(fnIn, fnOut);

    __log("Rename %s -> %s %s", fnIn, fnOut, (R)?"success":"error");

    return 0;
}
