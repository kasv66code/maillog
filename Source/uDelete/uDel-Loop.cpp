/* uDel-Loop.cpp									2019-09-17
*/


#include "uDel.hpp"


static time_t	To;
int	nDirs, nFiles;

// ------------------------------------------------------------------------------------------------
static bool	DirProcessing(const char *dest, const char *name_in_dest, void *var)
{
//    _printf("start DirProcessing %s%s", dest, name_in_dest);
    if (!name_in_dest || !*name_in_dest || !strcmp(name_in_dest, ".") || !strcmp(name_in_dest, "..")) {
	_printf("scop DirProcessing %s%s", dest, name_in_dest);
	return 1;
    }

    int Level = 0;
    if (var) {
	int *pLevel = (int *)var;
	Level = *pLevel;
    }

    time_t Tm = 0;
    if (_dirCheck2(dest, name_in_dest, &Tm)) {
	char dn[FileName_MaxSize];
	if (!_dirNameMake(dirNameMakeMode_makeOnly, dn, dest, name_in_dest, NULL)) {
	    _printf("scip Dir name %s%s", dest, name_in_dest);
	    return 1;
	}
//	_printf("loop Dir name %s", dn);

	if (onlyFile || Level > 0) {
	    if (Level > 0) Level--;
	    return _dirLoop(dn, DirProcessing, (void *)&Level);
	}

	if (Tm < To && _dirEmpty(dn)) {
	    if (_dirDelete(dn)) {
//		_printf("Dir delete %s", dn);
		nDirs++;
	    } else {
		_printf("Error! Dir delete %s", dn);
	    }
	} else {
	    bool R = _dirLoop(dn, DirProcessing, (void *)&Level);
	    _fileSetMtime(dn, Tm);
	    return R;
	}
	return 1;
    }

    if (_fileCheck2Null(dest, name_in_dest, &Tm)) {
	char fn[FileName_MaxSize];
	if (!_fileNameMake(fileNameMakeMode_makeOnly, fn, dest, name_in_dest, NULL)) return 1;
	if (Level > 0) {
	    return 1;
	}
	if (Tm < To) {
	    if (_fileDelete(fn)) {
//		_printf("File delete %s", fn);
		nFiles++;
	    }
	    else {
		_printf("Error! File delete %s", fn);
	    }
	}
	return 1;
    } else {
	_printf("Error! no File! %s%s", dest, name_in_dest);
    }
    return 1;
}


static void	makeTo(int Days)
{
    To = curr_time - (Days * _day_);
    struct tm tmc;
    localtime_r(&To, &tmc);

    tmc.tm_hour = 0;
    tmc.tm_min = 0;
    tmc.tm_sec = 1;

    To = mktime(&tmc);
}

// ------------------------------------------------------------------------------------------------
void	Loop(const char *Dir, int Level, int Days)
{
    makeTo(Days);
    _dirLoop(Dir, DirProcessing, (void *)&Level);
}
