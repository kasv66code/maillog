// uDel-Main.cpp									2019-09-17


#include "uDel.hpp"


bool	onlyFile;
time_t  curr_time;


static void	getWorkMode(const char *Sp, char &workMode)
{
    for (; *Sp; Sp++) {
	if (*Sp == '-' || *Sp == '/') continue;
	break;
    }
    switch (*Sp) {
	case 'h':
	    workMode = 'h';
	    break;
	case 'f':
	    workMode = 'f';
	    break;
    }
}

// ------------------------------------------------------------------------------------------------
static int	readValue(const char *S)
{
    for (; *S; S++) {
	if (*S >= '0' && *S <= '9') {
	    return convStrTo_i32(S);
	}
    }
    return -1;
}


static void	readParam(const char *S, int &Level, int &Days, const char **Dir)
{
    if (!S || !*S) return;

    if (*S == '-') {
	S++;
	switch (*S) {
	    case 'l':
		Level = readValue(S);
		break;
	    case 'd':
		Days = readValue(S);
		break;
	    case 'f':
		onlyFile = 1;
		break;
	}
    } else {
	*Dir = S;
    }
}


int	main(int argc, char** argv)
{
    __logInit(def_Folder_logs, _SCRIPT_NAME);
    setSignals(1);

    time(&curr_time);

    const char *Dir = NULL;
    int Level = 0;
    int Days = 0;

    for (int i=1; i<argc; i++) {
//	_printf("Arg[%d]: %s", i, argv[i]);
	readParam(argv[i], Level, Days, &Dir);
    }

    char currDir[FileName_MaxSize];
    if (!getcwd(currDir, sizeof(currDir)-1)) {
	_printf("ERROR! getcwd\n");
	return 0;
    }

    if (Level < 0 || Days < 0 || !Dir || !*Dir) {
	if (!Dir) Dir = "(null)";
	else if (!*Dir) Dir = "''";
	_printf("ERROR! params: Level=%d, Days=%d, Dir=%s\n", Level, Days, Dir);
	return 0;
    }

    char dn[FileName_MaxSize];
    if (*Dir == '/') {
	strcpy(dn, Dir);
    } else {
	_dirNameMake(dirNameMakeMode_makeOnly, dn, currDir, Dir, NULL);
    }

    if (!_dirCheck(dn)) {
	_printf("ERROR! Dir=%s\n", dn);
	return 0;
    }

    _printf("Start: Level=%d, Days=%d, onlyFile=%d, Dir=%s\n", Level, Days, (int)onlyFile, dn);

    Loop(dn, Level, Days);

    if (nDirs > 0 || nFiles > 0) {
	_printf("Stat Level=%d, Days=%d, onlyFile=%d, Dir=%s:\n\t\t\tndeleted Dirs=%d, nFiles=%d\n", Level, Days, (int)onlyFile, dn, nDirs, nFiles);
    }

    _printf("Stop");
    return 0;
}
