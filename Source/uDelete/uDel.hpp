// uDel.hpp										2019-09-17
//


#include "../cfgMC.hpp"


#if _WIN32
    char *getcwd(char *buf, size_t size);
#endif

#define _SCRIPT_NAME	"uDelete"


// uDel-Main.cpp
extern bool	onlyFile;
extern time_t	curr_time;

// uDel-Loop.cpp
extern int	nDirs, nFiles;
void	Loop(const char *Dir, int Level, int Days);

//#define _printf		printf
//#define _printf	__logErr
#define _printf	
