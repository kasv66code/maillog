// cfgMC.hpp										2021-01-15


#ifndef _cfgMC_hpp
#define _cfgMC_hpp


// �������� ����� �������� � ������
#define cfg_DISC_BASE		"/home/user/MailLog/"


// ************************************************************************************************


// ���������� �����: �������, cron, html, ������ �������, �����������-�������� - ��������� �������
#define def_Folder_Run			cfg_DISC_BASE "Run/"
// ������ � ��������� �������: ������, �������, �������, ���� � ��.
#define def_Folder_Data			cfg_DISC_BASE "Data/"
// ��������� �����: ����, lock-����� � ��.
#define def_Folder_logs			cfg_DISC_BASE "logs/"
// ������
#define def_Folder_Backup		cfg_DISC_BASE "Backup/"
// ��������� �����
#define def_Folder_tmp			cfg_DISC_BASE "tmp/"


// ���� log mail
#define FileName_Mail_log	def_Folder_tmp "maillog.txt"
// ���� ��� �������� �� �����
#define FileName_Emails_csv	def_Folder_Data "emails.csv"
// ���� ��� �������� ���������
#define FileName_Address	def_Folder_Data "emails.txt"
// ������, ������� ����� ������������
#define FileName_Ignore		def_Folder_Data "ignore.txt"


#include "Lib/types.hpp"
#include "Lib/exceptions.hpp"
#include "Lib/file.hpp"
#include "Lib/conv_numb.hpp"
#include "Lib/conv_time.hpp"
#include "Lib/conv_str.hpp"
#include "Lib/tools.hpp" 
#include "Lib/tree_str.hpp" 


#include <stdio.h>
#include <time.h>

#ifdef _WIN32
    int chdir(const char* path);
    #define strdup _strdup
    void sleep(unsigned long sec);
    void usleep(unsigned long usec);
    struct tm* localtime_r(const time_t* timep, struct tm* result);
    #define strcasecmp strcmp
    #define strncasecmp strncmp
    #define strcasestr strstr
#else
    #include <unistd.h>
    #include <sys/time.h>
#endif 


#define DIR_CHECK(n)	if (!_dirCheck(n)) {__logErr("dir check: %s", n); return 0;}
#define DIR_CREATE(n)	if (!_dirCheck_or_Create(n)) {__logErr("dir create: %s", n); return 0;}
#define FILE_CHECK(n)	if (!_fileCheck(n))  {__logErr("file not found: %s", n); return 0;}
#define msleep(ms)	usleep(ms*1000)



#endif
