// uFolders-Main.cpp									2020-10-26


#include "../cfgMC.hpp"


#define _SCRIPT_NAME	"uFolders"



// ------------------------------------------------------------------------------------------------
static bool	prepareFolders(void)
{
    DIR_CREATE(def_Folder_Run);
    DIR_CREATE(def_Folder_logs);
    DIR_CREATE(def_Folder_tmp);
    DIR_CREATE(def_Folder_Data);
    DIR_CREATE(def_Folder_Backup);

    return 1;
}


// ------------------------------------------------------------------------------------------------
int	main(int argc, char** argv)
{
    __logInit(def_Folder_logs, _SCRIPT_NAME);

    if (!prepareFolders()) {
        __logErr("prepareFolders error");
	printf("prepareFolders error!");
	return -1;
    }

    return 0;
}
