// mC.hpp										2021-01-13



#include "../cfgMC.hpp"


#define _SCRIPT_NAME	"MailCollect"


// mC-Main.cpp ------------------------------------------------------------------------------------

// mC-Mails.cpp -----------------------------------------------------------------------------------
class Mails {
    public:
	Mails(void);

	treeStr* T;
	treeStr* Ti;

	void	Stat(const char* email, time_t Tfirst, time_t Tlast, int Ndays, int Nsent);
	void	Add(const char* email, time_t t);

	void	saveAddress(void);
	void	saveReport(void);

	bool	loadLog(void);
	void	loadAddress(void);
	void	loadDomainsIgnore(void);

    private:
	bool	CheckEmail(const char* s);

	bool	ReadLineLog(char* s);
	void	ReadLineAddress(char* s);
	void	ReadLineIgnore(char* s);
	int	getMonthLog(const char* Month);

	time_t	currTime;
	int	currYear;
};
extern Mails eMails;

// mC-Loop.cpp ------------------------------------------------------------------------------------
void	Loop(void);