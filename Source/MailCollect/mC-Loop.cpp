﻿// mC-Loop.cpp									        2021-01-14


#include "mC.hpp"


#define LogMail_Status		" status=sent "
#define LogMail_To		" to=<"


// ------------------------------------------------------------------------------------------------
int	Mails::getMonthLog(const char* Month)
{
    if (Month && *Month) {
	for (int i = 0; i < 12; i++) {
	    if (!strcasecmp(Month, masName_month_EnShort[i]))
		return i + 1;

	}
    }
    return 0;
}


bool	Mails::ReadLineLog(char* s)
{
    if (!strstr(s, LogMail_Status)) return 0;

    char* sTo = strstr(s, LogMail_To);
    if (!sTo) {
	__log("Mails::ReadLineLog> Error parser to: %s", s);
	return 0;
    }

    sTo += sizeof(LogMail_To) - 1;
    char* sToEnd = strchr(sTo, '>');
    if (!sToEnd) return 0;
    *sToEnd = 0;

    char Month[10] = {0};
    int day=0, hour=-1, minute=-1, second=-1;
    if (sscanf(s, "%s %d %d:%d:%d:", Month, &day, &hour, &minute, &second) < 5) {
	__log("Mails::ReadLineLog> Error parser date: %s", s);
	return 0;
    }

    int month = getMonthLog(Month);
    if (month <= 0) {
	__log("Mails::ReadLineLog> Error parser month: %s; %s", Month, s);
	return 0;
    }

    time_t t = convTime_make(currYear, month, day, hour, minute, second);
    if (t > currTime) {
	t = convTime_make(currYear - 1, month, day, hour, minute, second);
    }

    Add(sTo, t);
    return 1;
}


bool	Mails::loadLog(void)
{
    FILE* f = fopen(FileName_Mail_log, "r");
    if (!f) return 0;

    int n = 0;
    char So[1000];
    for (; fgets(So, sizeof(So)-1,f);) {
	So[sizeof(So) - 1] = 0;

	if (ReadLineLog(So)) n++;
    }

    fclose(f);

    return (n > 0) ? 1 : 0;
}


// ------------------------------------------------------------------------------------------------
void	Mails::ReadLineAddress(char* s)
{
    char* email = nextField_tab(&s);
    int nDays = convStrTo_i32(nextField_tab(&s));
    int nSent = convStrTo_i32(nextField_tab(&s));
    time_t tFirst = convStrTo_timeNumb(nextField_tab(&s));
    time_t tLast = convStrTo_timeNumb(nextField_tab(&s));

    if (email && *email && nDays > 0 && nSent > 0 && tFirst > 0 && tLast >= tFirst) {
	Stat(email, tFirst, tLast, nDays, nSent);
    }
}


void	Mails::loadAddress(void)
{
    FILE* f = fopen(FileName_Address, "r");
    if (!f) return;

    char So[1000];
    for (; fgets(So, sizeof(So)-1,f);) {
	So[sizeof(So) - 1] = 0;

	ReadLineAddress(So);
    }

    fclose(f);
}


// ------------------------------------------------------------------------------------------------
void	Mails::ReadLineIgnore(char* s)
{
    if (s && *s) {
	s = convStr_clearSpace(s);
	if (s && *s) {
	    Ti->Insert(s);
	}
    }
}


void	Mails::loadDomainsIgnore(void)
{
    FILE* f = fopen(FileName_Ignore, "r");
    if (!f) return;

    char So[1000];
    for (; fgets(So, sizeof(So)-1,f);) {
	So[sizeof(So) - 1] = 0;

	ReadLineIgnore(So);
    }

    fclose(f);
}


// ------------------------------------------------------------------------------------------------
void	Loop(void)
{
    if (eMails.loadLog()) {
	eMails.loadAddress();
	eMails.loadDomainsIgnore();
	eMails.saveAddress();
	eMails.saveReport();
    }
}
