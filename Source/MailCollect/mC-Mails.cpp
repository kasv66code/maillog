﻿// mC-Mails.cpp									        2021-01-13


#include "mC.hpp"


Mails eMails;


// ------------------------------------------------------------------------------------------------
class Address {
    public:
	Address(const char* email);

	const char* eMail;
	time_t tFirst;
	time_t tLast;
	int nDays;
	int nSent;

	void	setStat(time_t Tfirst, time_t Tlast, int Ndays, int Nsent);
	void	addFromLog(time_t t);
};

Address::Address(const char* email)
{
    memset(this, 0, sizeof(*this));
    eMail = email;
}

void	Address::setStat(time_t Tfirst, time_t Tlast, int Ndays, int Nsent)
{
    if (Tlast >= tLast) {
	nDays = Ndays;
	nSent = Nsent;
	tLast = Tlast;
	tFirst = Tfirst;
    } else {
	tFirst = Tfirst;
	nDays += Ndays;
	nSent += Nsent;
    }
}

void	Address::addFromLog(time_t t)
{
    if (!tFirst) {
	tFirst = t;
	nDays = 1;
    }
    tLast = t;
    nSent++;
}



// ------------------------------------------------------------------------------------------------
Mails::Mails(void)
{
    memset(this, 0, sizeof(*this));

    T = new treeStr(treeStr::modeKeyCmp::utf8);
    Ti = new treeStr(treeStr::modeKeyCmp::utf8);

    time(&currTime);
    currYear = convTimeToYear(currTime);
}

void	Mails::Stat(const char* email, time_t Tfirst, time_t Tlast, int Ndays, int Nsent)
{
    nodeStr v = T->Insert(email);
    if (!v) return;
    Address* q;
    if (!v->data) {
	q = new Address(v->key);
	if (!q) return;
	v->data = q;
    } else {
	q = (Address*)v->data;
    }

    q->setStat(Tfirst, Tlast, Ndays, Nsent);
}

void	Mails::Add(const char* email, time_t t)
{
    nodeStr v = T->Insert(email);
    if (!v) return;
    Address* q;
    if (!v->data) {
	q = new Address(v->key);
	if (!q) return;
	v->data = q;
    } else {
	q = (Address*)v->data;
    }

    q->addFromLog(t);
}

void	Mails::saveAddress(void)
{
    int n = T->count();
    if (!n) return;

    const char* fn = FileName_Address;
    const char* fx = FileName_Address "~";
    FILE* f = fopen(fx, "w");
    if (f) {
	for (int i=0; i<n; i++) {
	    Address* q = (Address*)((*T)[i]->data);
	    if (q && q->eMail && *q->eMail && q->nDays > 0 && q->nSent > 0 && q->tFirst > 0 && q->tLast > 0) {
		fprintf(f, "%s\t%d\t%d\t%lld\t%lld\n", q->eMail, q->nDays, q->nSent, (i64)q->tFirst, (i64)q->tLast);
	    }
	}
	fclose(f);

	_fileRename(fx, fn);
    }
}


bool	Mails::CheckEmail(const char* s)
{
    if (!s || !*s) return 0;
    const char* Sa = strchr(s, '@');
    if (!Sa) return 0;

    Sa++;
    if (Ti->Find(Sa)) return 0;

    return 1;
}

void	Mails::saveReport(void)
{
    time_t minTime = currTime - (_year_ + 7);

    int n = T->count();
    if (!n) return;

    const char* fn = FileName_Emails_csv;
    const char* fx = FileName_Emails_csv "~";
    FILE* f = fopen(fx, "w");
    if (f) {
	fprintf(f, "\"e-mail\";\"days count\";\"mails count\";\"date first mail\";\"date last mail\";\n");
	for (int i = 0; i < n; i++) {
	    Address* q = (Address*)((*T)[i]->data);
	    if (q && CheckEmail(q->eMail) && q->nDays > 0 && q->nSent > 0 && q->tFirst > 0 && q->tLast > minTime && q->tLast >= q->tFirst) {
		fprintf(f, "%s;%d;%d;", q->eMail, q->nDays, q->nSent);

		char tBuff[50];
		convToStr_timeFormat(tBuff, q->tFirst, "%dd.%mm.%y %hh:%nn:%ss");
		fprintf(f, "\"%s\";", tBuff);
		convToStr_timeFormat(tBuff, q->tLast, "%dd.%mm.%y %hh:%nn:%ss");
		fprintf(f, "\"%s\"\n", tBuff);
	    }
	}
	fclose(f);

	_fileRename(fx, fn);
    }
}
