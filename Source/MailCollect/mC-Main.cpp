// mC-Main.cpp									        2021-01-13


#include "mC.hpp"


// ------------------------------------------------------------------------------------------------
static bool	prepareFolders(void)
{
    DIR_CHECK(def_Folder_Run);
    DIR_CREATE(def_Folder_logs);
    DIR_CREATE(def_Folder_tmp);
    DIR_CREATE(def_Folder_Data);
    DIR_CREATE(def_Folder_Backup);

    FILE_CHECK(FileName_Mail_log);

    return 1;
}


// ------------------------------------------------------------------------------------------------
int	main(int argc, char** argv)
{
    __logInit(def_Folder_logs, _SCRIPT_NAME);
    __dbgMaxThread();

    _dbgSet();
    if (!prepareFolders()) return -1;

    _dbgSet();
    setSignals(1);

    _dbgSet();
    __logDbg("Script start");

    try {
        Loop();
    }
    catch (...) {
        enaWork = 0;
	__logErr("Exception!");
    }
        
    _dbgClr();
    __logDbg("Stop");
    __logDbg(NULL);
    __logClose();

    return 0;
}
