/* tree_str.hpp										2020-04-14
*/


#ifndef	tree_str_hpp
#define	tree_str_hpp


#include "exceptions.hpp"
#include "mem.hpp"
#include <mutex>


typedef struct nodeStr_t {
    void		*data;		// ��������� ������ ������������
    int			numb;		// ���������� integer ��� ������������

    struct nodeStr_t	*next;		// ������ ��� ������������ �������
    int			hashKey;	// hash ��� key ��� �������� �������� ������������� key
    int			keyLen;		// ����� ������ key
    char		key[2];		// ���������� ���� - string
} *nodeStr;


typedef int	(*__sort_compare_nodeStr)(const nodeStr *, const nodeStr *);
typedef void	(*__freeData_nodeStr_t)(void *Qx, nodeStr v);
typedef void	(*__newData_nodeStr_t)(void *Qx, nodeStr v);


// --- ����� ������ Str ------------------------------------------------------------------
class treeStr {
    public:
	void *operator new(size_t size, class mem *m=NULL);
	void operator delete(void* p, mem* m);
	void operator delete(void *p);

	enum class modeKeyCmp {
	    none=0,
	    ascii,
	    utf8
	};

	treeStr(modeKeyCmp KeyCmp, void* _Qx = NULL, __newData_nodeStr_t _newData = NULL, __freeData_nodeStr_t _freeData = NULL);
	~treeStr(void);

	void	lockClose(void);		// ������� mutex, ���� �� �������������� �������� �����������

	void	treeFree(void);
	void	treeMasFree(void);

	nodeStr	Find(const char *key);			// ����� �� ���������� - ���������� ����� ����
	nodeStr	Insert(const char* key);		// �������� ���� - ���������� ����� ����
	void	Delete(const char* key);		// ������� �� ������ ���� ������ � ������� (���� ������� __free_nodeStringData)
	nodeStr	AppendDataInListLast(nodeStr q);	// ��������� ������� � ����� ������ (��� ������������ �������)
	nodeStr	AppendDataInListFirst(nodeStr q);	// ��������� ������� � ������ ������ (��� ������������ �������)

	// ��������� ������ �� ��������� ������ � ���������� ������� ������� �� ������� idx
	nodeStr	operator [] (int idx);

	int	count(void);

	// ��������� � ��������� ������ �� ��������� ������
	void	sort(__sort_compare_nodeStr fsort_compare);

	friend void	*_treeStrKeyNew(class _Mem* m, void* qu, const void* key);
	friend void     _treeStrKeyFree(class _Mem* m, void* qu, void* key);
    private:
	tree		*T;
	mem		*M;
	std::mutex	*lockT;

	nodeStr	makeNodeKey(const char* key);
	void	*copyNodeKey(const void*key);
	void	nodeFree(nodeStr v);

	modeKeyCmp		ModeKeyCmp;
	void			*Qx;		// ��������� ������������
	__newData_nodeStr_t	newData;	// ������� ��� �������� ������ �����
	__freeData_nodeStr_t	freeData;	// ������� ��� �������� ������ �����
};
// ------------------------------------------------------------------ ����� ������ Str ---



#endif
