/* tree_int.hpp										2020-04-13
*/


#ifndef	tree_int_hpp
#define	tree_int_hpp


#include "exceptions.hpp"
#include "mem.hpp"
#include <mutex> 


typedef struct nodeInt_t {
    void		*data;		// ��������� ������ ������������
    int			numb;		// ���������� integer ��� ������������

    struct nodeInt_t	*next;		// ������ ��� ������������ �������
    int			key;		// ���������� ����
} *nodeInt;


typedef int	(*__sort_compare_nodeInt)(const nodeInt *, const nodeInt *);
typedef void	(*__freeData_nodeInt_t)(void *Qx, nodeInt v);
typedef void	(*__newData_nodeInt_t)(void *Qx, nodeInt v);


// --- ����� ������ Int ------------------------------------------------------------------
class treeInt {
    public:
	void *operator new(size_t size, class mem *m=NULL);
	void operator delete(void* p, mem* m);
	void operator delete(void *p);

	treeInt(void* _Qx = NULL, __newData_nodeInt_t _newData = NULL, __freeData_nodeInt_t _freeData = NULL);
	~treeInt(void);

	void	    lockClose(void);		// ������� mutex, ���� �� �������������� �������� �����������

	void	    treeFree(void);
	void	    treeMasFree(void);

	nodeInt	    First(void);
	nodeInt	    Last(void);

	nodeInt	    Find(int key);			// ����� �� ���������� - ���������� ����� ����
	nodeInt	    Insert(int key);			// �������� ���� - ���������� ����� ����
	void	    Delete(int key);			// ������� �� ������ ���� ������ � ������� (���� ������� __free_nodeStringData)
	nodeInt	    AppendDataInListLast(nodeInt q);	// ��������� ������� � ����� ������ (��� ������������ �������)
	nodeInt	    AppendDataInListFirst(nodeInt q);	// ��������� ������� � ������ ������ (��� ������������ �������)

	// ��������� ������ �� ��������� ������ � ���������� ������� ������� �� ������� idx
	nodeInt	operator [] (int idx);

	int	    count(void);

	// ��������� � ��������� ������ �� ��������� ������
	void	    sort(__sort_compare_nodeInt fsort_compare);

	friend void	*_treeIntKeyNew(class _Mem* m, void* qu, const void* key);
	friend void     _treeIntKeyFree(class _Mem* m, void* qu, void* key);
    private:
	tree		*T;
	mem		*M;
	std::mutex	*lockT;

	void	*makeNodeKey(const void* key);
	void	makeNodeKey(nodeInt q, int key);

	void			*Qx;		// ��������� ������������
	__newData_nodeInt_t	newData;	// ������� ��� �������� ������ �����
	__freeData_nodeInt_t	freeData;	// ������� ��� �������� ������ �����
};
// ------------------------------------------------------------------ ����� ������ Int ---



#endif
