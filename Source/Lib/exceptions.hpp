// exceptions.hpp									2020-04-12
//			����������, ������� ����� � ������ � ���


#ifndef	exceptions_hpp
#define	exceptions_hpp

#include <errno.h>


// ������ ����� ��� ����� � ��� �������
void	__logInit(const char *dirName, const char *scriptName, int runIndex=0);
void	__logClose(void);

// ����� ��� - �������,��������,�������
void	__log(const char *fmt, ...);
void	__logError(const char *File, int Line, const char *fmt, ...);

#define __logErr(...)	__logError(__FILE__, __LINE__, __VA_ARGS__)

// ������ � ����� ��� ���� ���������� CGI-�������
void	__logEnv(void);

// ������� ��� - ��������� ���� ��� ������, ��������� ��� fmt==NULL (����� �� ���������) ��� ��� ����� �����
void	__logDbg(const char *fmt, ...);


class exceptions {
    public:
	exceptions(const char *file, int line) { __logDbg("Exceptions! file=%s, line=%d", file, line);}
};

#define __exThrow()		throw new exceptions(__FILE__,__LINE__)
#define	__exNull()		__exThrow()
#define	__exAlgorithm()		__exThrow()
#define	__exValue()		__exThrow()
#define	__exAlloc()		__exThrow()


#include <time.h>

void	getCurrTime_s(time_t &t);
void	getCurrTime_ms(time_t &t, int &ms);
void	getCurrTime_us(time_t &t, int &us);

// ������������� ������������ �������� + ������� �������, ���������� �� ��������������� ������������ -----------------------------------------------------
extern bool	enaWork;
void	setSignals(bool hup = 0, void(*fSignalReapChild)(int) = NULL, void(*fSignalFatal)(int) = NULL, void(*fSignalQuit)(int) = NULL, void(*fSignalIgnore)(int) = NULL);
/*
� ��� ������������ ��� ������� �������� __logDbg() ������ �������������� �������� � �� ������������� �� ��������:
	hup:
		SIGHUP
	fSignalReapChild:
		SIGCHLD
	fSignalFatal:
		SIGTRAP
		SIGIOT
		SIGEMT
		SIGSTKFLT;
		SIGCONT
		SIGPWR
		SIGSYS
		SIGILL
		SIGBUS
		SIGFPE
		SIGSEGV,
	fSignalQuit:
		SIGQUIT
		SIGINT
		SIGTERM
		SIGABRT
	fSignalIgnore:
		SIGUSR2
		SIGALRM
		SIGTSTP
		SIGTTIN
		SIGTTOU
		SIGURG
		SIGXCPU
		SIGXFSZ
		SIGVTALRM
		SIGPROF
		SIGPIPE
		SIGIO
------------------------------------------------------------------------------------------------------------------------------------------------------- */


// ����� ������������ ���������� ������������� ���������, ������ ������ �������������� ������ __dbg
int	__dbgMaxThread(int n=0);
void	__dbg(int idx, const char *file, int line);
void	__dbgClear(int idx);
int	__dbgNew(void);
void	__dbgDel(int &idx);
void	__dbgLog(void);

#define dbgSet(n)	__dbg(n,__FILE__,__LINE__)
#define dbgClr(n)	__dbgClear(n) 

#define _dbgSet()	__dbg(0,__FILE__,__LINE__)
#define _dbgClr()	__dbgClear(0) 



#endif
