/* tree_time.hpp								        2020-04-14
*/


#ifndef	tree_time_hpp
#define	tree_time_hpp


#include "exceptions.hpp"
#include "mem.hpp"
#include "types.hpp"
#include <time.h> 
#include <mutex> 


typedef struct nodeTime_t {
    void* data;		// ��������� ������ ������������
    int			numb;		// ���������� integer ��� ������������

    struct nodeTime_t* next;		// ������ ��� ������������ �������
    time_t		key;		// ���������� ����
} *nodeTime;


typedef int	(*__sort_compare_nodeTime)(const nodeTime*, const nodeTime*);
typedef void	(*__freeData_nodeTime_t)(void* Qx, nodeTime v);
typedef void	(*__newData_nodeTime_t)(void* Qx, nodeTime v);


// --- ����� ������ Time ------------------------------------------------------------------
class treeTime {
public:
    void* operator new(size_t size, class mem* m = NULL);
    void operator delete(void* p, mem* m);
    void operator delete(void* p);

    treeTime(void* _Qx = NULL, __newData_nodeTime_t _newData = NULL, __freeData_nodeTime_t _freeData = NULL);
    ~treeTime(void);

    void	    lockClose(void);		// ������� mutex, ���� �� �������������� �������� �����������

    void	    treeFree(void);
    void	    treeMasFree(void);

    nodeTime	    First(void);
    nodeTime	    Last(void);

    nodeTime	    Find(time_t key);			// ����� �� ���������� - ���������� ����� ����
    nodeTime	    Insert(time_t key);			// �������� ���� - ���������� ����� ����
    void	    Delete(time_t key);			// ������� �� ������ ���� ������ � ������� (���� ������� __free_nodeStringData)
    nodeTime	    AppendDataInListLast(nodeTime q);	// ��������� ������� � ����� ������ (��� ������������ �������)
    nodeTime	    AppendDataInListFirst(nodeTime q);	// ��������� ������� � ������ ������ (��� ������������ �������)

    // ��������� ������ �� ��������� ������ � ���������� ������� ������� �� ������� idx
    nodeTime	operator [] (int idx);

    int	            count(void);

    // ��������� � ��������� ������ �� ��������� ������
    void	    sort(__sort_compare_nodeTime fsort_compare);

    friend void*    _treeTimeKeyNew(class _Mem* m, void* qu, const void* key);
    friend void     _treeTimeKeyFree(class _Mem* m, void* qu, void* key);
private:
    tree* T;
    mem* M;
    std::mutex* lockT;

    void*       makeNodeKey(const void* key);
    void	makeNodeKey(nodeTime q, time_t key);

    void* Qx;		// ��������� ������������
    __newData_nodeTime_t	newData;	// ������� ��� �������� ������ �����
    __freeData_nodeTime_t	freeData;	// ������� ��� �������� ������ �����
};
// ------------------------------------------------------------------ ����� ������ Time ---



#endif
