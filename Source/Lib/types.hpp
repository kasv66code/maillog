// types.hpp										2020-03-20


#ifndef	types_hpp
#define	types_hpp

#ifndef _GNU_SOURCE
    #define _GNU_SOURCE
#endif // !_GNU_SOURCE


// types
typedef long long unsigned	u64;
typedef long long int		i64;
typedef unsigned int		u32;
typedef int			i32;
typedef unsigned short		u16;
typedef short			i16;
typedef unsigned char		u8;
typedef char			i8;


// define and const
#define FileName_MaxSize	256
#define Domain_MaxSize		100 
#define Directory_MaxDepth	7

#define kByte		1024
#define MByte		(1024*1024)

#define	_minute_	60
#define	_hour_		(60*_minute_)
#define	_day_		(24*_hour_)
#define	_week_		(7*_day_)
#define	_month_		(30*_day_)
#define	_year_		(365*_day_)


typedef int   (*__qsort_compare_t)(const void* pKeyA, const void* pKeyB);


#endif
