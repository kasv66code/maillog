/*  _mem.hpp										2020-04-12

			��������� ������������� ������
*/


#ifndef	_mem_hpp
#define	_mem_hpp


#include "mem-base.hpp"
#include "../exceptions.hpp"
#include "../types.hpp"
#include <stdlib.h>
#include <string.h>


// ************************************************************************************************

#define kByte			1024
#define TimesSizeOfBlock	4096		// ������ ��� ����� ���������� ������ TimesSizeOfBlock
#define TimesSizeOfFragment	16		// ������ ��� ��������� ���������� ������ TimesSizeOfFragment

#define defaultBlockSize	(16*TimesSizeOfBlock)
#define minBlockSize		(4*TimesSizeOfBlock)
#define maxBlockSize		(4096*TimesSizeOfBlock)
#define	sizeUstifyBlock(len)	((len % TimesSizeOfBlock || !len)?((len/TimesSizeOfBlock)+1)*TimesSizeOfBlock:len);
#define	sizeUstifyFragment(len)	((len % TimesSizeOfFragment || !len)?((len/TimesSizeOfFragment)+1)*TimesSizeOfFragment:len);
#define minFragmentSize		64

#define mem_SizeDefault_TableStructuresOfFragment	512
#define mem_SizeMin_TableStructuresOfFragment		64
#define mem_SizeMax_TableStructuresOfFragment		(256*kByte)

#define mem_SizeDefault_TableNodes		1024
#define mem_SizeMin_TableNodes			128
#define mem_SizeMax_TableNodes			(512*kByte)

// ************************************************************************************************


#define LockMem()	_Mem::lockMEM->lock()
#define unLockMem()	_Mem::lockMEM->unlock()
#define LockMem2()	_Mem::lockMEM2->lock()
#define unLockMem2()	_Mem::lockMEM2->unlock()



#endif
