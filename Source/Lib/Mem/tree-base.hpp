/* tree-base.hpp									2020-04-12
*/


#ifndef	tree_base_hpp
#define	tree_base_hpp
#include "../exceptions.hpp"


class _Mem;

// --- ����� �������� ������ ----------------------------------------------------------------------
typedef struct node_t {
    const void      *key;
    struct node_t   *left;
    struct node_t   *right;
    bool	    red;
} *node;

typedef int   (*__tree_key_compare_t)(const void* keyA, const void* keyB);
typedef void* (*__tree_key_copy_t)(class _Mem* m, void *qu, const void* key);
typedef void  (*__tree_key_free_t)(class _Mem* m, void *qu, void* key);
typedef int   (*__tree_sort_compare_t)(const void* pKeyA, const void* pKeyB);


class tree {
    public:
        tree(class _Mem *m, void *qu, bool destroy, __tree_key_compare_t fkey_compare, __tree_key_copy_t fkey_copy, __tree_key_free_t fkey_free);
        ~tree(void);

        void	    treeClear(void);	// ��������� �������� ��� ������������ ������
        void	    treeFree(void);	// �������� �������� � ������������� ������
        void	    treeMasFree(void);	// ����������� ������ �������, ����� �� ��� �� �����

        const void* treeFirst(void);
        const void* treeLast(void);

        const void* treeFind(const void* key);  	// ����� �� ���������� - ���������� ����� ����� ��� NULL, ���� ���� �� ������
        const void* treeFindGT(const void* key);	// ������ ��� ����� - ���������� ����� ����� ��� NULL, ���� ���� �� ������
        const void* treeInsert(const void* key);	// �������� ���� - ���������� ����� ����� (����������� ��� ��� ������������)
        const void* treeDelete(const void* key);	// ���������� ����� ����� ���������� ���� ��� NULL, ���� ���� �� ������ ��� ������ ������� �������� �����

        const void* operator [] (int idx);

        void	    treeSort(__tree_sort_compare_t fsort_compare);

        int	    treeCount(void);
    private:
        _Mem        *M;
        void        *Qu;
        const void  **mas;
        node	    root;

        bool	update;
        bool	destroy;

        int	count;
        int	i;

        __tree_key_compare_t	key_compare;
        __tree_key_copy_t	key_copy;
        __tree_key_free_t	key_free;

        node	treeNodeNew();
        void	treeNodeFree(node);
        void	treeDestroy(node);
        void	treeMasMake(void);
        void	treeMasWalk(node);

        const void  *treeIndex(int idx);        // ���������� ������� ������� �� ������� idx
};
// ---------------------------------------------------------------------- ����� �������� ������ ---



#endif
