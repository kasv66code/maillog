/*  _Mem-base.hpp									2020-04-12

			��������� ������������� ������
*/


#ifndef	mem_base_hpp
#define	mem_base_hpp


#include "tree-base.hpp"
//#include <new>


#include <mutex> 
using namespace std;


#define enaStatistic	0


// ************************************************************************************************

// ��������� �������� �����
typedef struct mem_Block_s {
    struct mem_Block_s	*block_Prev;		// ����� ����������� ����� ������
    void		*remain_Address;	// ����� ������ ���������� ���������������� ������
    int			 remain_Size;		// ������ ������� ���������������� ������ � �����
    void		*fragment_Last;		// ����� ��������� ���������� ��������� � ���� ����� (��������� ������ ��� ����������, �������� ����� - ���� ���� ������� ���������������� ������ � �����)
} mem_Block_t;


// ��������� �������� ���������
typedef struct mem_StructureOfFragment_s {
    void	*fragment_Address;	// ����� ��������� ������ �������� size

    int		fragment_Size;		// ������ ����������� ��������� ������
    int		fragment_Len;		// >0 ������������ ������ ������; 0 �� ������������, ����������

    mem_Block_t	*block_His;		// ����� ��������� ������ �����

    struct mem_StructureOfFragment_s	*prevFragmentOfHisBlock, *nextFragmentOfHisBlock;	// ��������� ��� ������ �������� ���������� ���������� (� �������� ������ �����!)
    struct mem_StructureOfFragment_s	*prevFreeOfSize, *nextFreeOfSize;			// ��������� ��� ������ �������� ������������� ������ ������ ������� (� �������� ������ ���� ������)
    struct mem_StructureOfFragment_s	*next_ListOfFreeStructureOfFragment;
} mem_StructureOfFragment_t;


// ������� ����������
typedef struct mem_TableStructuresOfFragment_s {
    struct mem_TableStructuresOfFragment_s	*prevTableStructuresOfFragment;	// ����� ���������� ������� ����������
    int				masCountUsed;			// ���������� �������������� ��������� �������
    int				masSize;
    mem_StructureOfFragment_t	*mas;
} mem_TableStructuresOfFragment_t;


// ������� node
typedef struct mem_TableNodes_s {
    struct mem_TableNodes_s	*prevTableNodes;	// ����� ���������� ������� node
    int			masCountUsed;		// ���������� �������������� ��������� �������
    int			masSize;
    struct node_t	*mas;
} mem_TableNodes_t;

// ************************************************************************************************



class _Mem {
    public:
	_Mem(void);		// ������� ������ ��-���������
	// ������������ � ��������� �������� ������
	_Mem(int _blockSizeFirst);
	_Mem(int _blockSizeFirst, int _tblFragmentSizeFirst, int _tblNodeSizeFirst);
	_Mem(int _blockSizeFirst, int _blockSizeNext, int _tblFragmentSizeFirst, int _tblFragmentSizeNext, int _tblNodeSizeFirst, int _tblNodeSizeNext);
	~_Mem(void);

	void	 memClear(void);	// ��������� �������� ��� ������������ ������
	void	 memFree(void);		// ������� ������������ ���� ���������� ������

	void	*mem_Alloc(int size);	// �������� ������
	void	*mem_Calloc(int size);	// �������� ������ � ��������� 0
	void	 mem_Free(void *addr);	// ���������� ������
	int	 mem_Size(void *addr);	// ���������� ���������� ������ ��������� ������
	int	 mem_Len(void *addr);	// ���������� ����������� ������ ��������� ������
	char	*mem_StrDup(const char *Str);	// ������� �������� ������

#if enaStatistic==1
	const char	*name;
#endif

    private:
	mutex	*lockMEM;
	mutex	*lockMEM2;

	void	*_mem_Alloc(int size);	// �������� ������
	int	 _mem_Size(void *addr);	// ���������� ���������� ������ ��������� ������
	int	 _mem_Len(void *addr);	// ���������� ����������� ������ ��������� ������

	// �������� -------------------------------------------------------------------------------
	mem_Block_t	*block;		// ������� (���������) ����
	// ������� ������
	int blockSizeFirst, blockSizeNext, tblFragmentSizeFirst, tblFragmentSizeNext, tblNodeSizeFirst, tblNodeSizeNext;

	tree	*treeAlloc;	// ������ ���������� ����������
	tree	*treeFree;	// ������ ������������� ��������

	mem_StructureOfFragment_t	*list_StructureOfFragmentFree;	// ������ ������������� �������� �������� ����������
	mem_TableStructuresOfFragment_t	*table_StructuresOfFragment;	// ������� (���������) ������� �������� ����������

	node			list_NodeFree;		// ������ ������������� node
	mem_TableNodes_t	*table_Nodes;		// ������� (���������) ������� �������� node

	// ������ ---------------------------------------------------------------------------------
	void	*address_Offset(void *addr, int offset);	// ������� �������� ����� �� offset ����

	void	TableNodes_Create(void);	// ������� ����� ������� �������� node
	void	TableNodes_CloseAll(void);	// ���������� ������ ���� ������ �������� node

	mem_StructureOfFragment_t	*StructOfFragment_New();	// �������� ��������� ��� �������� ���������
	void	StructOfFragment_Free(mem_StructureOfFragment_t *);	// ���������� ��������� ��� �������� ��������� (������� � ������ ��������� ��������)
	void	TableStructuresOfFragment_Create(void);			// ������� ����� ������� �������� ����������
	void	TableStructuresOfFragment_CloseAll(void);		// ���������� ������ ���� ������ �������� ����������

	void	block_Create(int uLen);		// ��������� ������ �����
	void	block_CloseAll(void);		// ���������� ������ ���� ������

	void	fragment_Free(mem_StructureOfFragment_t *);
	void	fragment_ListFreeOfSize_Append(mem_StructureOfFragment_t *);
	void	fragment_ListFreeOfSize_Delete(mem_StructureOfFragment_t *);

	int	_check_IntegrationWithRemain(mem_StructureOfFragment_t *q);
	void	_IntegrationWithRemain(mem_StructureOfFragment_t *q);

	int	_check_IntegrationWithAdjacent(mem_StructureOfFragment_t *q);
	void	_IntegrationWithAdjacent(mem_StructureOfFragment_t *q);

	mem_StructureOfFragment_t	*fragment_LoadFromFree(int size);
	mem_StructureOfFragment_t	*fragment_Create(int size);
	mem_StructureOfFragment_t	*fragment_MakeInBlock(int size);

	void	memInit(void);


#if enaStatistic==1
	// ���������� -----------------------------------------------------------------------------
	int	countNodes_Curr;
	int	countFragments_Curr;
	int	memorySize_Curr;
	int	countNodesTbl_Curr;
	int	countFragmentsTbl_Curr;
	int	countBlocks_Curr;

	int	countNodes_Max;
	int	countFragments_Max;
	int	memorySize_Max;
	int	countNodesTbl_Max;
	int	countFragmentsTbl_Max;
	int	countBlocks_Max;
#endif

	friend class tree;
    private:
	node	node_New();		// �������� ��������� ��� node
	void	node_Free(node);	// ���������� ��������� ��� node
};



#endif
