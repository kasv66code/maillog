/* conv_numb.hpp									2020-04-25
*/



#ifndef	_conv_numb_hpp
#define	_conv_numb_hpp


#include "types.hpp"


// �������������� ������ � �����
i32	convStrTo_i32(const char *str);
u32	convStrTo_u32(const char* str);
i32	convStrTo_x32(const char* str);

i64	convStrTo_i64(const char* str);
u64	convStrTo_u64(const char* str);
i64	convStrTo_x64(const char* str);

double	convStrTo_double(const char* str);



// �������������� ����� � ������
int	convToStr_i32(char *buff, i32 d);
int	convToStr_u32(char *buff, u32 d);
int	convToStr_x32(char *buff, i32 d);

int	convToStr_i64(char *buff, i64 d);
int	convToStr_u64(char *buff, u64 d);
int	convToStr_x64(char *buff, i64 d);

int	convToStr_double(char *buff, double d);
int	convToStr_double_AccuracyCount(char *buff, double d, int n);
int	convToStr_double_AccuracyAuto(char *buff, double d);

int	convToStr_StrNumber(char *buff, const char *StrNumb);



#endif
