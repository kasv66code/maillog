/* conv_time.hpp									2020-04-25
*/



#ifndef	_conv_time_hpp
#define	_conv_time_hpp


#include "types.hpp"
#include <time.h>


// �������������� �������
int	convToStr_timeGMT(char* buff, time_t t);
time_t	convStrTo_timeGMT(const char* str);

int	convToStr_timeNumb(char *buff, time_t t);	// ����������� ����� � �������� ������
time_t	convStrTo_timeNumb(const char *str);

int	convTimeToYear(time_t t);	// ���������� ���
int	convTimeToMonth(time_t t);	// ���������� �����
int	convTimeToDay(time_t t);	// ���������� ���� ������
int	convTimeToHour(time_t t);	// ���������� ���
int	convTimeToMin(time_t t);	// ���������� ������
int	convTimeToSec(time_t t);	// ���������� �������
int	convTimeToDayOfWeek(time_t t);	// ���������� ���� ������

void	convTimeToNumb(time_t t, int &year, int &month, int &day, int &hour, int &minute, int &second);
void	convTimeToDate(time_t t, int &year, int &month, int &day);
void	convTimeToTime(time_t t, int &hour, int &minute, int &second);

time_t	convStrTo_timeCompile(const char *Sdate, const char *Stime);

time_t	convTime_make(int year, int month, int day, int hour, int minute, int second);
time_t	convTime_makeGMT(int year, int month, int day, int hour, int minute, int second);

extern const char **masName_month_En;			// January, ..
extern const char **masName_month_EnShort;		// Jan, ..
extern const char **masName_weekDay_EnShort;		// Sun, ..

extern const char **masName_month_RuUTF;		// ������, ..
extern const char **masName_month_RuUTFShort;		// ���, ..
extern const char **masName_month_RuUTFGenitive;	// ������, ..

extern const char **masName_month_RuCP1251;		// ������, ..
extern const char **masName_month_RuCP1251Short;	// ���, ..
extern const char **masName_month_RuCP1251Genitive;	// ������, ..

int	convToStr_timeFormat(char *buff, time_t t, const char *strFormat=NULL, const char **month=NULL);	// ��������� ��������� ������������� �������
// ������ ������:
//	%y - ������ ���, %yy - ��� ��������� ����� ����
//	%m - ����� [1..12], %mm - ��� ����� ������ [01..12], %ms - ���������� ������������ ������ �� ������� ����� month[]
//	%d - ���� ������ [1..31], %dd - ��� ����� ��� ������ [01..31]
//	%h - ��� [0..23], %hh - ��� ����� ���� [00..23]
//	%n - ������ [0..59], %nn - ��� ����� ����� [00..59]
//	%s - ������� [0..59], %ss - ��� ����� ������ [00..59]
//	%% - ����������� ������ '%'

#define	StringTimeFormat_Default	"%dd.%mm.%y %hh:%nn:%ss"



#endif
