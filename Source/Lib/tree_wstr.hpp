/* tree_wstr.hpp									2020-04-14
*/


#ifndef	tree_wstr_hpp
#define	tree_wstr_hpp


#include "exceptions.hpp"
#include "mem.hpp"
#include <mutex> 


typedef struct nodeWStr_t {
    void		*data;		// ��������� ������ ������������
    int			numb;		// ���������� integer ��� ������������

    struct nodeWStr_t	*next;		// ������ ��� ������������ �������
    int			hashKey;	// hash ��� key ��� �������� �������� ������������� key
    int			keyLen;		// ����� ������ key
    wchar_t		key[2];		// ���������� ���� - string
} *nodeWStr;


typedef int	(*__sort_compare_nodeWStr)(const nodeWStr *, const nodeWStr *);
typedef void	(*__freeData_nodeWStr_t)(void *Qx, nodeWStr v);
typedef void	(*__newData_nodeWStr_t)(void *Qx, nodeWStr v);


// --- ����� ������ Str ------------------------------------------------------------------
class treeWStr {
    public:
	void *operator new(size_t size, class mem *m=NULL);
	void operator delete(void* p, mem* m);
	void operator delete(void *p);

	enum class modeKeyCmp {
	    none=0,
	    lower
	};

	treeWStr(modeKeyCmp KeyCmp, void* _Qx = NULL, __newData_nodeWStr_t _newData = NULL, __freeData_nodeWStr_t _freeData = NULL);
	~treeWStr(void);

	void	lockClose(void);		// ������� mutex, ���� �� �������������� �������� �����������

	void	treeFree(void);
	void	treeMasFree(void);

	nodeWStr	Find(const wchar_t*key);			// ����� �� ���������� - ���������� ����� ����
	nodeWStr	Insert(const wchar_t* key);		// �������� ���� - ���������� ����� ����
	void		Delete(const wchar_t* key);		// ������� �� ������ ���� ������ � ������� (���� ������� __free_nodeWStringData)
	nodeWStr	AppendDataInListLast(nodeWStr q);	// ��������� ������� � ����� ������ (��� ������������ �������)
	nodeWStr	AppendDataInListFirst(nodeWStr q);	// ��������� ������� � ������ ������ (��� ������������ �������)

	// ��������� ������ �� ��������� ������ � ���������� ������� ������� �� ������� idx
	nodeWStr	operator [] (int idx);

	int		count(void);

	// ��������� � ��������� ������ �� ��������� ������
	void		sort(__sort_compare_nodeWStr fsort_compare);

	friend void	*_treeWStrKeyNew(class _Mem* m, void* qu, const void* key);
	friend void     _treeWStrKeyFree(class _Mem* m, void* qu, void* key);
    private:
	tree		*T;
	mem		*M;
	std::mutex	*lockT;

	nodeWStr	makeNodeKey(const wchar_t* key);
	void		*copyNodeKey(const void*key);
	void		nodeFree(nodeWStr v);

	modeKeyCmp		ModeKeyCmp;
	void			*Qx;		// ��������� ������������
	__newData_nodeWStr_t	newData;	// ������� ��� �������� ������ �����
	__freeData_nodeWStr_t	freeData;	// ������� ��� �������� ������ �����
};
// ------------------------------------------------------------------ ����� ������ Str ---



#endif
