/* conv_str.hpp										2020-04-30
*/



#ifndef	_conv_str_hpp
#define	_conv_str_hpp


// �������������� �����
int	convStr_toLow_Anscii(char *buff, const char *str);
int	convStr_toLow_UTF(char *buff, const char *str);
int	convWStr_toLow(wchar_t *buff, const wchar_t *str);

char	*convStr_clearSpace(char *s);

const char	*convStr_nybble(const char *p, int *n);	// �������� � n ����������������� ����� � ���������� ��������� �� �������� ����� ����� ������
int	convStr_urlDecode(char *buff, const char *url);

int	convStr_urlToDomain(char *buff, const char *url, bool www);
int	convStr_urlToDomainLevel2(char *buff, const char *url);

int	strlenUTF(const char *str);

bool	punycodeDecode(const char *So, char *Sn);
bool	punycodeEncode(const char *So, char *Sn);

int	cmpStr_Low_UTF(const char *str1, const char* str2);


#endif
