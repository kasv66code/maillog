/* list.hpp										2020-04-15
			����� ��� ���������� �������
*/


#ifndef	list_hpp
#define	list_hpp


#include "exceptions.hpp"
#include "mem.hpp"
#include <mutex> 



typedef struct nodeList_t {
    int			numb;		// ���������� integer ��� ������������
    void		*data;		// ��������� ������ ������������
    struct nodeList_t	*prev, *next;	// ��������� ��� ���������� ���������������� ������
} *nodeList;


typedef void	(*__freeData_nodeList_t)(void* Qx, nodeList v);
typedef void	(*__newData_nodeList_t)(void* Qx, nodeList v);
typedef int	(*__sort_compare_nodeList)(const nodeList *, const nodeList *);


// --- ����� �������� ������ ----------------------------------------------------------------------
class list {
    public:
	void *operator new(size_t size, mem *m=NULL);
	void operator delete(void* p, mem* m);
	void operator delete(void* p);

	list(void* _Qx = NULL, __newData_nodeList_t _newData = NULL, __freeData_nodeList_t _freeData = NULL);
	~list(void);

	void   lockClose(void);		// ������� mutex, ���� �� �������������� �������� �����������

	nodeList	AddFirst(void);			// �������� ���� � ������ ������
	nodeList	AddLast(void);			// �������� ���� � ����� ������

	nodeList	AddBefore(nodeList);		// �������� ���� ����� ��������� ������
	nodeList	AddAfter(nodeList);		// �������� ���� ����� �������� ������

	void		Delete(nodeList);		// ������� ����, ���� ������� freeData, �� ������ � �������

	nodeList	getFirst(void);			// ���� � ������ ������
	nodeList	getLast(void);			// ���� � ������ ������

	// ��������� ������ �� ��������� ������ � ���������� ������� ������� �� ������� idx
	nodeList	operator [] (int idx);

	void		sort(__sort_compare_nodeList fsort_compare);	// ��������� � ��������� ������ �� ��������� ������

	int		count(void);


    private:
	mem		*M;
	int		Count;
	nodeList	first, last;
	std::mutex	*lockL;

	nodeList	*mas;
	bool		update;
	int		sizeMasMem;

	void			*Qx;		// ��������� ������������
	__newData_nodeList_t	newData;	// ������� ��� �������� ������
	__freeData_nodeList_t	freeData;	// ������� ��� �������� ������

	nodeList	NewNode(void);
	void		MasMake(void);
	nodeList	getIndex(int idx);
};
// ---------------------------------------------------------------------- ����� �������� ������ ---



#endif
