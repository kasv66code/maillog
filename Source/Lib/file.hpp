/* file.hpp									2014.01.06
*/


#ifndef	_file_hpp
#define	_file_hpp


#include <time.h>
#include <string.h>
#include "types.hpp"
#include "mem.hpp"


// ------------------------------------------------------------------------------------------------

typedef enum {
    fileOpenMode_read,			// ��������� ����
    fileOpenMode_write,			// ������������ ����, ������� ���������� ��������� (���������, ���� ����� ������������ lock)
    fileOpenMode_update,		// ������ �������� �������� �����
    fileOpenMode_update_or_create,	// �������� �������� ����� ��� ������� ����, ���� ���� �� ����������
    fileOpenMode_create,		// ������� ����� ����, ���� ��� ���������� - ������
    fileOpenMode_append			// �������� � ����� �����
} fileOpenMode;


bool	_fileOpen(int *fd, const char *filename, fileOpenMode mode, int pMode=0644);
int	_fileClose(int fd);
bool	_fileLock(int fd, bool exclusive, bool no_block);
void	_fileUnLock(int fd);

bool	 _fileDelete(const char *filename);
bool	 _fileRename(const char *fn_old, const char *fn_new);

bool	 _fileGetStat(int fd, i64 *len, time_t *mtime);
bool	 _fileGetStat(int fd, int *len, time_t *mtime);
bool	 _fileGetStat(const char *filename, i64 *len, time_t *mtime);
bool	 _fileGetStat(const char *filename, int *len, time_t *mtime);

bool	 _fileSetMtime(const char *filename, time_t t);

bool	 _fileSetLen(int fd, i64 length);
bool	 _fileSetLen(const char *filename, i64 length);

i64	 _fileGetPos(int fd);
bool	 _fileSetPos(int fd, i64 pos);

int	 _fileRead(int fd, void *buff, int length);
int	 _fileWrite(int fd, const void *buff, int length);

bool	 _fileCheck(const char *filename, time_t *Tm = NULL);
bool	 _fileCheck2(const char *dirname, const char *filename, time_t *Tm = NULL);

bool	 _fileCheckNull(const char *filename, time_t *Tm = NULL);
bool	 _fileCheck2Null(const char *dirname, const char *filename, time_t *Tm = NULL);

typedef enum {
    fileCopyMode_step,		// ���� ���� ����, ����������
    fileCopyMode_checkNew,	// ���������, ���� ������������ ���� ������, �� ��������
    fileCopyMode_all,		// �������� � ����� ������
} fileCopyMode;

bool	 _fileCopy(const char *fn_from, const char *fn_to, fileCopyMode mode=fileCopyMode_all);

void*   FileToMem(const char* fn, int& len, int additive = 0);
bool	MemToFile(const char* fn, const void* s, int len);
char*   FileToStr(const char* fn, int& len, int additive = 0);
bool	StrToFile(const char* fn, const char* s);
void*   FileToMem(mem *M, const char* fn, int& len, int additive = 0);
char*   FileToStr(mem *M, const char* fn, int& len, int additive = 0);

typedef enum {
    fileNameMakeMode_makeOnly,		// ������ ������� ������ � ������ �����
    fileNameMakeMode_dirCheck,		// ������� ������ ����� ����� � ��������� ������� �����
    fileNameMakeMode_dirCheck_or_Create	// ������� ������ ����� ����� � ��������� ������� ����� � ������� ��, ���� ��� �����������
} fileNameMakeMode;

// _fileNameMake(fileNameMakeMode, filefullname, dirname, filename, NULL);
// _fileNameMake(fileNameMakeMode, filefullname, dirname1, dirname2, filename, NULL);
// _fileNameMake(fileNameMakeMode, filefullname, dirname1, dirname2, dirname3, filename, NULL);
bool	 _fileNameMake(fileNameMakeMode mode, char *filefullname, ...);

// ------------------------------------------------------------------------------------------------

inline time_t	 _fileGetMtime(int fd) {time_t t=0; _fileGetStat(fd, (int *)NULL, &t); return t;}
inline time_t	 _fileGetMtime(const char *filename) {time_t t=0; _fileGetStat(filename, (int *)NULL, &t); return t;}
inline i64	 _fileGetLen(int fd) { i64 l = 0; if (_fileGetStat(fd, &l, NULL)) return l; else return -1;}
inline i64	 _fileGetLen(const char *filename) {i64 l=0; _fileGetStat(filename, &l, NULL); return l;}
inline i64	 _fileWriteStr(int fd, const char *str) {if (!str || !*str) {return 0;} return _fileWrite(fd, str, (int)strlen(str));}


//*************************************************************************************************


bool	 _dirCheck(const char *dirname, time_t *Tm=NULL);
bool	 _dirCheck_or_Create(const char *dirname, int pMode = 0755);
bool	 _dirCheck2(const char *dirname1, const char *dirname2, time_t *Tm=NULL);

bool	 _dirEmpty(const char *dirname);
bool	 _dirLoop(const char *dirname, bool (*fc)(const char *dest, const char *name_in_dest, void *var), void *var);

bool	 _dirClear(const char *dirname, bool enaRmDir=0);
bool	 _dirDelete(const char *dirname);

bool	 _dirCopy(const char *dn_from, const char *dn_to, fileCopyMode mode);


typedef enum {
    dirNameMakeMode_makeOnly,		// ������ ������� ������ � ������ �����
    dirNameMakeMode_dirCheck,		// ������� ������ ����� ����� � ��������� ������� �����
    dirNameMakeMode_dirCheck_or_Create	// ������� ������ ����� ����� � ��������� ������� ����� � ������� ��, ���� ��� �����������
} dirNameMakeMode;

// _dirNameMake(dirNameMakeMode, dirfullname, dirname1, dirname2, NULL);
// _dirNameMake(dirNameMakeMode, dirfullname, dirname1, dirname2, dirname3, NULL);
bool	 _dirNameMake(dirNameMakeMode mode, char *dirfullname, ...);

// ------------------------------------------------------------------------------------------------

class _fGetS {
    public:
	_fGetS(int _buffSize=0);
	~_fGetS(void);

	bool	fileOpen(const char *fileFullName);
	void	fileClose(void);
	char	*GetStr(char c);
	char	*GetStr(void) {return GetStr('\n');}

    private:
	char *buffer;
	int count, start;
	int fileLength, countRead;

	int fd;
	int buffSize;
};




#endif
