/* tree_double.hpp								        2020-04-14
*/


#ifndef	tree_double_hpp
#define	tree_double_hpp


#include "exceptions.hpp"
#include "mem.hpp"
#include "types.hpp"
#include <time.h> 
#include <mutex> 


typedef struct nodeDouble_t {
    void* data;		// ��������� ������ ������������
    int			numb;		// ���������� integer ��� ������������

    struct nodeDouble_t* next;		// ������ ��� ������������ �������
    double		key;		// ���������� ����
} *nodeDouble;


typedef int	(*__sort_compare_nodeDouble)(const nodeDouble*, const nodeDouble*);
typedef void	(*__freeData_nodeDouble_t)(void* Qx, nodeDouble v);
typedef void	(*__newData_nodeDouble_t)(void* Qx, nodeDouble v);


// --- ����� ������ Time ------------------------------------------------------------------
class treeDouble {
public:
    void* operator new(size_t size, class mem* m = NULL);
    void operator delete(void* p, mem* m);
    void operator delete(void* p);

    treeDouble(void* _Qx = NULL, __newData_nodeDouble_t _newData = NULL, __freeData_nodeDouble_t _freeData = NULL);
    ~treeDouble(void);

    void	    lockClose(void);		// ������� mutex, ���� �� �������������� �������� �����������

    void	    treeFree(void);
    void	    treeMasFree(void);

    nodeDouble	    First(void);
    nodeDouble	    Last(void);

    nodeDouble	    Find(double key);			// ����� �� ���������� - ���������� ����� ����
    nodeDouble	    Insert(double key);			// �������� ���� - ���������� ����� ����
    void	    Delete(double key);			// ������� �� ������ ���� ������ � ������� (���� ������� __free_nodeStringData)
    nodeDouble	    AppendDataInListLast(nodeDouble q);	// ��������� ������� � ����� ������ (��� ������������ �������)
    nodeDouble	    AppendDataInListFirst(nodeDouble q);	// ��������� ������� � ������ ������ (��� ������������ �������)

    // ��������� ������ �� ��������� ������ � ���������� ������� ������� �� ������� idx
    nodeDouble	operator [] (int idx);

    int	            count(void);

    // ��������� � ��������� ������ �� ��������� ������
    void	    sort(__sort_compare_nodeDouble fsort_compare);

    friend void*    _treeDoubleKeyNew(class _Mem* m, void* qu, const void* key);
    friend void     _treeDoubleKeyFree(class _Mem* m, void* qu, void* key);
private:
    tree* T;
    mem* M;
    std::mutex* lockT;

    void*       makeNodeKey(const void* key);
    void	makeNodeKey(nodeDouble q, double key);

    void* Qx;		// ��������� ������������
    __newData_nodeDouble_t	newData;	// ������� ��� �������� ������ �����
    __freeData_nodeDouble_t	freeData;	// ������� ��� �������� ������ �����
};
// ------------------------------------------------------------------ ����� ������ Time ---



#endif
