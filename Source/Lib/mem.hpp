/*  mem.hpp										2020-04-12

			��������� ������������� ������
*/


#ifndef	mem_hpp
#define	mem_hpp

#include <new>

class _Mem;
class tree;


class mem {
    public:
	mem(int k=1);
	~mem(void);

	void	mClear(void);	// ��������� �������� ��� ������������ ������
	void	mFree(void);	// ������������ ������

	void	*Alloc(int size);	// �������� ������
	void	*Calloc(int size);	// �������� ������ � ��������� 0
	void	Free(void *addr);	// ���������� ������
	int	Size(void *addr);	// ���������� ���������� ������ ��������� ������
	int	Len(void *addr);	// ���������� ����������� ������ ��������� ������
	char	*StrDup(const char *Str);	// ������� �������� ������


	friend class treeInt;
	friend class treeInt64;
	friend class treeTime;
	friend class treeDouble;
	friend class treeStr;
	friend class treeWStr;
    private:
	_Mem* M;
};



#endif
