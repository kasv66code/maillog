/* tree_int64.hpp									2020-04-14
*/


#ifndef	tree_int64_hpp
#define	tree_int64_hpp


#include "exceptions.hpp"
#include "mem.hpp"
#include "types.hpp"
#include <mutex> 


typedef struct nodeInt64_t {
    void* data;		// ��������� ������ ������������
    int			numb;		// ���������� integer ��� ������������

    struct nodeInt64_t* next;		// ������ ��� ������������ �������
    i64			key;		// ���������� ����
} *nodeInt64;


typedef int	(*__sort_compare_nodeInt64)(const nodeInt64*, const nodeInt64*);
typedef void	(*__freeData_nodeInt64_t)(void* Qx, nodeInt64 v);
typedef void	(*__newData_nodeInt64_t)(void* Qx, nodeInt64 v);


// --- ����� ������ Int64 ------------------------------------------------------------------
class treeInt64 {
public:
    void* operator new(size_t size, class mem* m = NULL);
    void operator delete(void* p, mem* m);
    void operator delete(void* p);

    treeInt64(void* _Qx = NULL, __newData_nodeInt64_t _newData = NULL, __freeData_nodeInt64_t _freeData = NULL);
    ~treeInt64(void);

    void	    treeFree(void);
    void	    treeMasFree(void);

    void	    lockClose(void);		// ������� mutex, ���� �� �������������� �������� �����������

    nodeInt64	    First(void);
    nodeInt64	    Last(void);

    nodeInt64	    Find(i64 key);			// ����� �� ���������� - ���������� ����� ����
    nodeInt64	    Insert(i64 key);			// �������� ���� - ���������� ����� ����
    void	    Delete(i64 key);			// ������� �� ������ ���� ������ � ������� (���� ������� __free_nodeStringData)
    nodeInt64	    AppendDataInListLast(nodeInt64 q);	// ��������� ������� � ����� ������ (��� ������������ �������)
    nodeInt64	    AppendDataInListFirst(nodeInt64 q);	// ��������� ������� � ������ ������ (��� ������������ �������)

    // ��������� ������ �� ��������� ������ � ���������� ������� ������� �� ������� idx
    nodeInt64	operator [] (int idx);

    int	            count(void);

    // ��������� � ��������� ������ �� ��������� ������
    void	    sort(__sort_compare_nodeInt64 fsort_compare);

    friend void*    _treeInt64KeyNew(class _Mem* m, void* qu, const void* key);
    friend void     _treeInt64KeyFree(class _Mem* m, void* qu, void* key);
private:
    tree* T;
    mem* M;
    std::mutex* lockT;

    void*       makeNodeKey(const void* key);
    void	makeNodeKey(nodeInt64 q, i64 key);

    void* Qx;		// ��������� ������������
    __newData_nodeInt64_t	newData;	// ������� ��� �������� ������ �����
    __freeData_nodeInt64_t	freeData;	// ������� ��� �������� ������ �����
};
// ------------------------------------------------------------------ ����� ������ Int64 ---



#endif
