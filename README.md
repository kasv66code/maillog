## Сбор адресатов из логов Postfix

Этот проект предназначен для формирования базы адресатов на основе анализа логов почтового сервера.  
На данный момент обрабатываются логи Postfix.

Основной скрипт - **MailCollect** - запускается с помощью **cron** раз в сутки ( Run/cron-day.sh ). Он формирует два файла: *emails.txt* и *emails.csv*  
Файл **emails.txt** - внутренний файл для накопления адресатов. Его формат:
```
email \t days count \t mails count \t date first mail \t date last mail \n
```
где
* days count - количество дней, когда было отправление на этот адрес
* mails count - общее количество отправлений
* date first mail - дата и время первого отправления
* date last mail - дата и время последнего отправления

Файл **emails.csv** - отчет по адресатам за последние 12 месяцев. Его формат:
```
email ; days count ; mails count ; date first mail ; date last mail \n
```
Предполагается, что *emails.csv* будет открываться с помощью Exel для анализа и формирования отчетов.

Для исключения "своих" доменов предназначен файл **ignore.txt** . В нем на одной строке указывается один домен. Доменов может быть много.
Почтовые адреса этих доменов не будут попадать в отчет *emails.csv* .
Файл *ignore.txt* должен быть в каталоге Data/

Раз в неделю файл *emails.csv* можно отправлять с помощью **cron** исполнителю для анализа адресатов или для формирования базы адресов для рассылки.  
Пример такого файла "cron-week.sh":
```sh
#!/bin/bash
cd "/home/user/MailLog/" || exit
file=/home/user/MailLog/Data/emails.csv
if [ -f $file ] 
then
echo '12 month mailing list from <you_domain>' | mail -s 'List of recipients' -a $file -r postmaster@<you_domain> <you_mail>
fi
```

Проект размещен в каталоге /home/user/MailLog/  
При необходимости этот каталог можно изменить - в файле cfgMC.hpp
```c++
#define cfg_DISC_BASE		"/home/user/MailLog/"
```

Для формирования скриптов достаточно запустить **make** в каталоге Source  
В результате должно сформироваться четыре скрипта:
* MailCollect - скрипт для обработки лога и формирования адресатов
* uDate - утилита переименования backup
* uDelete - утилита удаления ненужных файлов (для сборки мусора)
* uFolders - утилита создает рабочие каталоги

